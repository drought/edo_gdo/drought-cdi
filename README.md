# Combined Drought Indicator (CDI) Repository

## Description
Welcome to the Combined Drought Indicator (CDI) Repository. This Indicator is designed to identify areas potentially suffering from agricultural drought, areas where vegetation is already affected by drought conditions, and areas in the recovery process returning to normal conditions after a drought episode. The CDI is a comprehensive method that integrates various key factors, including the Standardized Precipitation Index (SPI), Soil Moisture Anomalies (SMA), and the Fraction of Absorbed Photosynthetically Active Radiation (fAPAR).

The CDI categorizes drought impact into six distinct levels:
1. **Watch**: Indicates a relevant precipitation shortage.
2. **Warning**: This level is reached when the precipitation shortage is accompanied by a soil moisture anomaly.
3. **Alert**: Declared when both precipitation shortage and soil moisture anomaly are present, along with an anomaly in vegetation condition.
4. **Temporary Soil Moisture Recovery**: Occurs when, after a drought episode, soil moisture conditions improve but not sufficiently to declare the episode closed.
5. **Temporary fAPAR Recovery**: This level indicates that, following a drought episode, vegetation conditions have improved but not enough to consider the drought episode closed.
6. **Full Recovery**: Achieved when meteorological conditions, soil moisture, and vegetation return to their normal states.

## Data Source
For more detailed information and access to the data used by this tool, please visit the [JRC Data Catalogue](https://data.jrc.ec.europa.eu/dataset/afa8a5ee-5473-439a-b062-ffdaedc38b2d).

## Code Availability
The code for the Combined Drought Indicator is currently in the process of being published and will be available soon. We are working diligently to ensure that the code is robust, user-friendly, and well-documented. Stay tuned for updates.

## Contributing
While the main code is not yet published, we welcome contributions in the form of suggestions, bug reports, and discussions regarding the methodology and its applications. Please feel free to open an issue or submit a pull request for any auxiliary tools or documentation you might find useful for this project.

## License
Details regarding the licensing of the code and data will be provided alongside the code release.

## Contact
For any inquiries, please open an issue in this repository, and we will get back to you as soon as possible.

---

Thank you for your interest in the Combined Drought Indicator. We believe this tool will be invaluable in understanding and mitigating the impacts of agricultural droughts.
